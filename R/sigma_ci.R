#' Confidence Interval for the residual standard deviation of a linear model
#' 
#' Compute a Confidence Interval 
#' @param x An object of class \code{lm}
#' @param alpha 
#'
#' @return A data.frame with 1 line and variables \code{parameter}, \code{point_est},
#' \code{ll} and \code{hh}.
#' @export
#'
#' @references See, for instance,
## https://stats.stackexchange.com/questions/497439/calculating-confidence-intervals-for-the-variance-of-the-residuals-in-r
#'
#' @examples
#' sigma_ci(lm(y ~ 1, data.frame(y = rnorm(1e3, sd = 2))), alpha = 0.05)
sigma_ci <- function(x, alpha) {
  stopifnot(inherits(x, "lm"))
  sum_sq <- sum(x$residuals ** 2)
  ci <- sqrt(
    sum_sq / qchisq(p = c(1, 0) + c(-1, 1) * alpha / 2, df = x$df.residual)
  )
  data.frame(
    parameter = "sigma",
    point_est = summary(x)$sigma,
    ll = ci[1], hh = ci[2]
  )
}
