
# crashbayes <img src="man/figures/logo.png" align="right" style="width: 200px;" alt="A visualization of the posterior distribution in between the prior and the likelihood." />

<!-- badges: start -->
[![CRAN_Status_Badge](http://www.r-pkg.org/badges/version/crashbayes)](https://cran.r-project.org/package=crashbayes)
[![pipeline status](https://forgemia.inra.fr/umr-astre/training/crashbayes/badges/main/pipeline.svg)](https://forgemia.inra.fr/umr-astre/training/crashbayes/-/commits/main)
[![crashbayes status badge](https://cirad-astre.r-universe.dev/badges/crashbayes)](https://cirad-astre.r-universe.dev)
<!-- badges: end -->

`crashbayes` contains training materials for a short (~ 3h), practical course in Bayesian Regression Modelling.

You can browse the materials on line at https://umr-astre.pages.mia.inra.fr/training/crashbayes/articles/crashbayes.html, even without installing the package.

Installing `crashbayes` automatically triggers the installation of all the R-packages used in the training, all the examples and code used during the course in the form of a _vignette_, and all the example datasets and helper functions, with proper documentation and help pages.



## Installation

In GNU/Linux or MacOS, you need a few system libraries and tools used by some
of the R-packages required by `crashbayes`. Specifically, `curl`, `xml2`,
`openssl`, `fontconfig` and `cmake`. The packaging depends on the system. E.g.

```
deb: libcurl4-openssl-dev libxml2-dev libssl-dev libfontconfig1-dev cmake
rpm: libcurl-devel libxml2-devel openssl-devel fontconfig-devel cmake
brew: openssl freetype cmake
```

You can install the development version of `crashbayes` like so:

```r
install.packages('crashbayes', repos = c('https://cirad-astre.r-universe.dev', 'https://cloud.r-project.org'))
```

## Usage

Load the package and open the main vignette

``` r
vignette('crashbayes')
```

To follow the code along (without needing to copy-and-paste), open the source code of the main vignette

``` r
file.edit(system.file('doc/crashbayes.R', package = "crashbayes"))
```

or browse the [code on line](https://umr-astre.pages.mia.inra.fr/training/crashbayes/articles/crashbayes.R).
